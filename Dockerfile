FROM python:3.7-alpine AS rz4z-back-pack
MAINTAINER Dmitrii Dmitriev useroid@gmail.com

WORKDIR /opt/rz4z-back-pack

# Update apk index and install system dependencies
RUN set -x \
    && apk update \
    && apk add --no-cache \
        build-base \
        python3-dev \
        postgresql-dev

COPY requirements/env.txt requirements/linux.txt requirements/

RUN set -x \
# Update and/or install python packages
    && pip install --upgrade --no-cache-dir -r requirements/env.txt -r requirements/linux.txt \
# Build wheels packages
    && pip wheel -w wheels -r requirements/linux.txt

CMD ["true"]


FROM python:3.7-alpine AS rz4z-back
MAINTAINER Dmitrii Dmitriev useroid@gmail.com

ENV GUNICORN_GID 1000
ENV GUNICORN_UID 1000

ENV SOURCE_DIR /opt/rz4z-back
ENV DATA_DIR /srv/rz4z-back
ENV DATA_DIR_STATIC $DATA_DIR/static
ENV DATA_DIR_MEDIA $DATA_DIR/media

WORKDIR $SOURCE_DIR

RUN set -x \
    && apk update \
    && apk add --no-cache \
# PostgreSQL packages
        libpq \
# Add user and group for gunicorn
    && addgroup -g $GUNICORN_GID gunicorn && adduser -DH -u $GUNICORN_UID -G gunicorn gunicorn

# Get requirements and built wheels
COPY requirements requirements
COPY --from=rz4z-back-pack /opt/rz4z-back-pack/wheels wheels

RUN set -x \
# Install previously built wheels and delete stale stuff
    && pip install --no-cache-dir -r requirements/env.txt \
    && pip install --no-cache-dir --no-index --find-links wheels -r requirements/linux.txt \
    && pip install --no-cache-dir -r requirements/any.txt \
    && rm -rf wheels requirements

# Copy project sources
COPY --chown=gunicorn:gunicorn src .
COPY --chown=gunicorn:gunicorn docker-entrypoint.sh .

RUN set -x \
# Create data directories and apply permissions
    && mkdir -p $DATA_DIR_STATIC $DATA_DIR_MEDIA \
# Adjust file permissions
    && chown gunicorn:gunicorn . $DATA_DIR_MEDIA \
    && chmod +x docker-entrypoint.sh \
# Collect static
    && python manage.py collectstatic --noinput -v 0

# Create default data volume
VOLUME $DATA_DIR

EXPOSE 8000

ENTRYPOINT ["/opt/rz4z-back/docker-entrypoint.sh"]

CMD ["run"]
