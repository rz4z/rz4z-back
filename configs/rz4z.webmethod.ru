upstream gunicorn {
    server localhost:8000 fail_timeout=0;
}

server {
    server_name rz4z.webmethod.ru  www.rz4z.webmethod.ru rz4z.ru www.rz4z.ru;

    charset utf-8;
    client_max_body_size 50m;
    access_log /var/log/nginx/rz4z.access.log;
    error_log /var/log/nginx/rz4z.error.log;
    index index.html index.htm index.nginx-debian.html;

    location = /favicon.ico {
        access_log off;
        log_not_found off;
    }

    location / {
        root /srv/rz4z-front;
        try_files $uri $uri/ =404;
    }

    location /media/ {
        alias /srv/rz4z-back/media/;
    }

    location /static/ {
        alias /srv/rz4z-back/static/;
        add_header Cache-Control "max-age=86400";
    }

    location ~ ^/(api/|admin) {
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header Host $http_host;

        proxy_redirect off;
        proxy_pass http://gunicorn;
    }

    listen [::]:443 ssl; # managed by Certbot
    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/webmethod.ru/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/webmethod.ru/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot
}

server {
    if ($host = rz4z.webmethod.ru) {
        return 301 https://$host$request_uri;
    } # managed by Certbot

    listen 80;
    listen [::]:80;
    server_name rz4z.webmethod.ru;
    return 404; # managed by Certbot
}

