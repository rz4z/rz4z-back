#!/bin/sh
set -e

# Adjust data directories
mkdir -p $DATA_DIR_MEDIA $DATA_DIR_STATIC
chown $GUNICORN_UID:$GUNICORN_GID $DATA_DIR_MEDIA

# Register standart run modes
if [[ "$1" = "run" ]]; then
    exec gunicorn rz4z.wsgi --config python:rz4z.gunicorn
elif [[ "$1" = "manage" ]]; then
    exec python manage.py
elif [[ "$1" = "migrate" ]]; then
    exec python manage.py migrate --noinput
elif [[ "$1" = "test" ]]; then
    exec python manage.py test
elif [[ "$1" = "collectstatic" ]]; then
    exec python manage.py collectstatic --noinput -v 0
fi

# To be able to apply a custom command
exec "$@"
