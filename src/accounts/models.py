import re

from django.contrib.auth.models import AbstractUser
from django.core.exceptions import ValidationError

RE_CALLSIGN = re.compile(r'[A-Z0-9]{1,3}\d[A-Z0-9]{0,3}[A-Z]', flags=re.IGNORECASE)


class Account(AbstractUser):
    error_messages = {
        'callsign': 'Укажите корректный позывной.'
    }

    def clean(self):
        super().clean()
        if not RE_CALLSIGN.match(self.username):
            raise ValidationError({'username': [self.error_messages['callsign']]})
        self.username = self.username.upper()
