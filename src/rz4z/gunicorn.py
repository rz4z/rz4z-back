import os

bind = ["0.0.0.0:8000"]

workers = 3
worker_class = 'gevent'

user = os.environ.get('GUNICORN_UID')
group = os.environ.get('GUNICORN_GID')

if not user:
    del user
if not group:
    del group
